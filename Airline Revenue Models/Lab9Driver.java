package lab9;

/*
Steven Kinneair
CS 210
3/24/2019
Lab 9
Lab9Driver.java
*/

import java.util.*;
import java.io.*;

/**
 * Driver for choosing a revenue model for a given airline's flights.
 */
public class Lab9Driver {
	/**
	 * Asks for user input and calls populatePlanes, airlineCode, and chooseRevenueModel using the user given information.
	 * @throws FileNotFoundException
	 */
	public static void main(String[] args) throws FileNotFoundException{
		//Input for populatePlanes method
		Scanner planeInput = new Scanner(System.in);
		Scanner fileInput = new Scanner(new File("InputData.csv"));
		
		//Input for airlineCode method
		Scanner codeInput = new Scanner(System.in);
		System.out.println("Enter an airline code to be processed: ");
		String planeCode = codeInput.nextLine();
		
		//Input for chooseRevenueModel method
		System.out.println("Revenue Models");
		System.out.println("(1) Single Price");
		System.out.println("(2) Two Classes");
		System.out.println("(3) Multi Class");
		Scanner modelInput = new Scanner(System.in);
		System.out.println("Select a Revenue Model(1,2,3): ");
		int model = modelInput.nextInt();
		
		ArrayList<String[]> planes = new ArrayList<String[]>();
		ArrayList<Flight> flightDetails = new ArrayList<Flight>();
		
		//Method calls
		populatePlanes(planes, fileInput);
		airlineCode(planes, flightDetails, planeCode);
		chooseRevenueModel(flightDetails, model);
		
		
		planeInput.close();
		codeInput.close();
		modelInput.close();

	}
	
	/**
	 * Reads through a file and adds each line of the file into a list named planes.
	 * @param planes represents an ArrayList of an empty string array. 
	 * @param fileInput represents a file containing flight information. 
	 * @throws FileNotFoundException
	 */
	public static void populatePlanes(ArrayList<String[]> planes, Scanner fileInput) throws FileNotFoundException{
		while(fileInput.hasNextLine()) {
			String line = fileInput.nextLine();
			String[] flights = line.split(",");
			planes.add(flights);
		}
		fileInput.close();
	}
	
	/**
	 * Creates a new Flight based on the users given airline code. The flight consists of all of the details that come with the airline code.
	 * @param planes represents an ArrayList of String arrays containing all flights in the file. 
	 * @param flightDetails represents an empty ArrayList of flights.
	 * @param planeCode represents the user given planeCode.
	 */
	public static void airlineCode(ArrayList<String[]> planes, ArrayList<Flight> flightDetails, String planeCode) {
		for(String[] i: planes) {
			Airline airline = new Airline(i[0]);
			if (i[0].equals(planeCode)) {
				Flight flight = new Flight(airline, i[1], Integer.parseInt(i[2]), i[3], i[4], Integer.parseInt(i[5]), Integer.parseInt(i[6])); 
				flightDetails.add(flight);
			}
		}
	}
	
	/**
	 * Allows the user to choose which revenue model they would like to use.
	 * @param flightDetails represents an ArrayList of all flights containing a given airline code.
	 * @param model represents the users input for which model they would like to use.
	 */
	public static void chooseRevenueModel(ArrayList<Flight> flightDetails, int model) {
		switch (model) {
		case 1:
			model = 1;
			SinglePrice s1 = new SinglePrice();
			System.out.println("Single Price Revenue Model:");
			System.out.println("$" + s1.getRevenue(flightDetails));
			break;
		case 2:
			model = 2;
			TwoClasses t1 = new TwoClasses();
			System.out.println("Two Classes Revenue Model:");
			System.out.println("$" + t1.getRevenue((flightDetails)));
			break;
		case 3:
			model = 3;
			MultiClass m1 = new MultiClass();
			System.out.println("Multi Class Revenue Model:");
			System.out.println("$" + m1.getRevenue(flightDetails));
			break;
		default:
			System.out.println("Not a valid model number. Select again.");
			chooseRevenueModel(flightDetails, model);
			break;
			
		}
	}
}
