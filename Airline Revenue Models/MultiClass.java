package lab9;

import java.util.ArrayList;

/*
Steven Kinneair
CS 210
3/24/2019
Lab 9
MultiClass.java
*/


/**
 * Calculates revenue based on the Multi Class model
 */
public class MultiClass implements RevenueModel {
	private double firstClassTicketPrice = 300 * 4;
	private double businessClassTicketPrice = 300 * 1.5;
	private double coachTicketPrice = 300 * .75;
	private double costToFlyPlane = 50000 * 1.2;
	private ArrayList<Double> revenueList = new ArrayList<Double>();
	
	/**
	 * Calculates each individual flights revenue and places them in a ArrayList.
	 * @param flightDetails represents a flight and its details.
	 */
	public void calculateRevenue(ArrayList<Flight> flightDetails) {
		for(int i = 0; i < flightDetails.size(); i++) {
			double calculatedRevenue = (firstClassTicketPrice * .1 * flightDetails.get(i).getPassengers())
				+ (businessClassTicketPrice * .2 * flightDetails.get(i).getPassengers())
				+ (coachTicketPrice * .7 * flightDetails.get(i).getPassengers())
				- costToFlyPlane;
			revenueList.add(calculatedRevenue);
		}
	}
	
	/**
	 * Takes a flight containing the details of that flight and calculates the revenue for a particular airline.
	 * @param flightDetails represents a flight and its details.
	 * @return Revenue, represented by a long containing the total revenue for a given airline.
	 */
	public long getRevenue(ArrayList<Flight> flightDetails) {
		int Revenue = 0;
		calculateRevenue(flightDetails);
		for(double totalRevenue: revenueList) {
			Revenue += totalRevenue;
		}
		return (long) Revenue;
	}
}
