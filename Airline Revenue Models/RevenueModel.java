package lab9;

/*
Steven Kinneair
CS 210
3/24/2019
Lab 9
RevenueModel.java
*/

import java.util.*;

/**
 * RevenueModel is a interface that specifies the getRevenue method.
 */
public interface RevenueModel {
	int ticketPrice = 300;
	int costToFlyPlane = 50000;
	long getRevenue(ArrayList<Flight> flightDetails);
}
