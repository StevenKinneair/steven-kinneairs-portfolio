package lab9;

/*
Steven Kinneair
CS 210
3/24/2019
Lab 9
SinglePrice.java
*/

import java.util.*;

/**
 * Calculates revenue based on the Single Price model
 */
public class SinglePrice implements RevenueModel{
	private ArrayList<Integer> revenueList = new ArrayList<Integer>();
	
	/**
	 * Calculates each individual flights revenue and places them in a ArrayList.
	 * @param flightDetails represents a flight and its details.
	 */
	public void calculateRevenue(ArrayList<Flight> flightDetails) {
		for(int i = 0; i < flightDetails.size(); i++) {
			int calculatedRevenue = (ticketPrice * flightDetails.get(0).getPassengers()) - costToFlyPlane;
			revenueList.add(calculatedRevenue);
		}
	}
	/**
	 * Takes a flight containing the details of that flight and calculates the revenue for a particular airline.
	 * @param flightDetails represents a flight and its details.
	 * @return Revenue, represented by a long containing the total revenue for a given airline.
	 */
	public long getRevenue(ArrayList<Flight> flightDetails) {
		int Revenue = 0;
		calculateRevenue(flightDetails);
		for(int totalRevenue: revenueList) {
			Revenue += totalRevenue;
		}
		return (long) Revenue;
	}
}
