package lab9;

/*
Steven Kinneair
CS 210
3/24/2019
Lab 9
Airline.java
*/

public class Airline{
    private String code;
    
    public Airline(String c){
        code = c;
    }
    
    public String getCode(){
        return code;
    }
}