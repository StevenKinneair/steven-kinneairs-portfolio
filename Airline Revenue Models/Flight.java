package lab9;

/*
Steven Kinneair
CS 210
3/24/2019
Lab 9
Flight.java
*/

public class Flight{
    private Airline airline;
    private String airplane;
    private int flightNum;
    private String depAirport;
    private String arrAirport;
    private int time;
    private int passengers;
    
    public Flight(Airline a, String pl, int fn, String dep, String arr, int t, int p){
        airline = a;
        airplane = pl;
        flightNum = fn;
        depAirport = dep;
        arrAirport = arr;
        time = t;
        passengers = p;
    }
    
    public int getPassengers(){
        return passengers;
    }
	
	public Airline getAirline(){
		return airline;
	}
	
	public String getAirplane(){
		return airplane;
	}
	
	public int getFlightNumber(){
		return flightNum;
	}
	
	public String getDeparture(){
		return depAirport;
	}
	
	public String getArrival(){
		return arrAirport;
	}
	
	public int getTime(){
		return time;
	}
}