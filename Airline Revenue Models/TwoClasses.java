package lab9;

import java.util.ArrayList;

/*
Steven Kinneair
CS 210
3/24/2019
Lab 9
TwoClasses.java
*/

/**
 * Calculates revenue based on the Two Classes model.
 */
public class TwoClasses implements RevenueModel {
	private double businessClassTicketPrice = 300 * 1.5;
	private double coachTicketPrice = 300 * .75;
	private double costToFlyPlane = 50000 * 1.1;
	private ArrayList<Double> revenueList = new ArrayList<Double>();

	
	/**
	 * Calculates each individual flights revenue and places them in a ArrayList.
	 * @param flightDetails represents a flight and its details.
	 */
	public void calculateRevenue(ArrayList<Flight> flightDetails) {
		for(int i = 0; i < flightDetails.size(); i++) {
			double calculatedRevenue = (businessClassTicketPrice * .2 * flightDetails.get(i).getPassengers())
					+ (coachTicketPrice * .8 * flightDetails.get(i).getPassengers()) 
					- costToFlyPlane;
			revenueList.add(calculatedRevenue);
		}
	}
	
	/**
	 * Takes a flight containing the details of that flight and calculates the revenue for a particular airline.
	 * @param flights represents a flight and its details.
	 * @return Revenue, represented by a long containing the total revenue for a given airline.
	 */
	public long getRevenue(ArrayList<Flight> flightDetails) {
		int Revenue = 0;
		calculateRevenue(flightDetails);
		for(double totalRevenue: revenueList) {
			Revenue += totalRevenue;
		}
		return (long) Revenue;
	}
}
